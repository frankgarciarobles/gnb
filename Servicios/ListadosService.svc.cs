﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using GNB.Logica;
using log4net;
using System.Xml.Linq;

namespace GNB.ListadosService
{
    
    public class ListadosService : GNB.Logica.DataContracts.IListadosService
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
     
        public string ListarTransacciones()
        {
            try
            {
                GNB.Logica.Listados listados = new GNB.Logica.Listados();
                XElement lista = listados.ListarTransacciones();

                return GNB.Logica.Utilidades.Serializar(lista, typeof(XElement));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Se ha producido un error en ListarTransacciones: {0}", ex.ToString());
                return string.Format("Se ha producido un error en ListarTransacciones: {0}", ex.ToString());
            }
        }

        public string ListarTasas()
        {
            try
            {
                GNB.Logica.Listados listados = new GNB.Logica.Listados();
                XElement lista = listados.ListarTasas();

                return GNB.Logica.Utilidades.Serializar(lista, typeof(XElement));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Se ha producido un error en ListarTasas: {0}", ex.ToString());
                return string.Format("Se ha producido un error en ListarTasas: {0}", ex.ToString());
            }
        }

        public string ListarTransaccionesXProducto(string sku)
        {
            try
            {
                GNB.Logica.Listados listados = new GNB.Logica.Listados();
                GNB.Logica.DataContracts.ListaTransacciones lista = new GNB.Logica.DataContracts.ListaTransacciones();
                lista = listados.ListadoPorProducto(sku);

                return GNB.Logica.Utilidades.Serializar(lista, typeof(GNB.Logica.DataContracts.ListaTransacciones));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Se ha producido un error en ListarTransaccionesXProducto: {0}", ex.ToString());
                return string.Format("Se ha producido un error en ListarTransaccionesXProducto: {0}", ex.ToString());
            }
        }

    }
}
