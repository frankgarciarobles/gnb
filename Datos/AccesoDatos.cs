﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Web;
using System.Net;
using System.Xml.Serialization;
using System.IO;
using log4net;

namespace GNB.Datos
{
    public class AccesoDatos
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>Recupera un archivo de la url indicada</summary>
        /// <returns>XElement con el contenido del fichero</returns>
        /// <param name="ruta">string con el URI de dónde recuperar el archivo</param>
        /// <param name="archivo">string con el nombre del archivo</param>
        public static XElement RecuperaArchivo(string ruta, string archivo)
        {
            string rutaDestino = "";

            try
            {
                // Recuperamos el archivo
                WebClient myWebClient = new WebClient();
                myWebClient.DownloadFile(ruta + archivo, archivo);
            }
            catch (Exception e)
            {
                Log.Debug("No se ha podido recuperar el fichero y se usará la última versión del archivo.");
            }

            try
            {
                //Lo copiamos en la ruta de la aplicación para mantener el archivo en caso de que no se pueda recuperar el arhivo actualizado
                rutaDestino = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase) + "\\" + archivo;
                rutaDestino = new Uri(rutaDestino).LocalPath;
                if (System.IO.File.Exists(rutaDestino)) { System.IO.File.Delete(rutaDestino); } //Si existe el fichero se elimina
                System.IO.File.Copy(archivo, rutaDestino);
                
                //Devolvemos el documento
                XElement doc = XElement.Load(rutaDestino);
                return doc;
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Se ha producido un error al recuperar el archivo. Detalle del error: {0}", e.Message);
                return null;
            }
        }        
    }
}
