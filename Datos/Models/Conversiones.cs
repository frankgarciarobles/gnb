﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GNB.Datos.Models
{
    public class Conversiones
    {
        [Serializable]
        [XmlRoot(ElementName = "rates")]
        public class Rates
        {
            [XmlElement("rate")]
            public List<ListRates> Rate { get; set; }
        }

        [XmlRoot(ElementName = "rate")]
        public class ListRates
        {
            [XmlAttribute(AttributeName = "from")]
            public string From { get; set; }

            [XmlAttribute(AttributeName = "to")]
            public string To { get; set; }

            [XmlAttribute(AttributeName = "rate")]
            public string Rate { get; set; }
        }
    }
}
