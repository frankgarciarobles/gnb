﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GNB.Datos.Models
{
    public class Transacciones
    {
        [Serializable]
        [XmlRoot(ElementName = "transactions")]
        public class Transactions
        {
            [XmlElement("transaction")]
            public List<ListTransactions> Transaction { get; set; }
        }

        [XmlRoot(ElementName = "transaction")]
        public class ListTransactions
        {
            [XmlAttribute(AttributeName = "sku")]
            public string Sku { get; set; }

            [XmlAttribute(AttributeName = "amount")]
            public string Amount { get; set; }

            [XmlAttribute(AttributeName = "currency")]
            public string Currency { get; set; }
        }
    }
}
