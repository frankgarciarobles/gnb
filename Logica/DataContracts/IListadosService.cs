﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;


namespace GNB.Logica.DataContracts
{
    
    // Interface del servicio web de listados
    [ServiceContract]
    public interface IListadosService
    {

        [OperationContract]
        string ListarTransacciones();

        [OperationContract]
        string ListarTasas();

        [OperationContract]
        string ListarTransaccionesXProducto(string sku);

    }
    
    // Clases para la devolución de las transacciones
    [DataContract]
    public class ListaTransacciones
    {
        [DataMember]
        public transaction[] transactions;

        [DataMember]
        public decimal total;
    }

    [DataContract]
    public class transaction
    {
        [DataMember]
        public string sku;

        [DataMember]
        public string amount;

        [DataMember]
        public string currency;
    }

    // Clase para la devolución de las transacciones originales
    [DataContract]
    public class ListaTransaccionesOriginales
    {
        [DataMember]
        public transaction[] transactions;
    }

    // Clase para la devolución de las tasas
    [DataContract]
    public class ListaTasas
    {
        [DataMember]
        public Tasa[] tasa;
    }

    [DataContract]
    public class Tasa
    {
        [DataMember]
        public string from;

        [DataMember]
        public string to;

        [DataMember]
        public string rate;
    }

}
