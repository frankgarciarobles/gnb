﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GNB.Datos;
using System.Xml.Serialization;
using System.IO;
using System.Web;
using System.Configuration;
using System.Xml.Linq;
using System.Globalization;
using log4net;

namespace GNB.Logica
{

    public class Listados
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>Devuelve las transacciones para un producto convirtiendo a la moneda configurada</summary>
        /// <returns>Un objeto ListaTransacciones con las transacciones</returns>
        /// <param name="sku">Producto del cual se quieren obtener las transacciones</param>
        public GNB.Logica.DataContracts.ListaTransacciones ListadoPorProducto(string sku)
        {

            try
            {
                XElement Transacciones;
                XElement Conversiones;
                decimal total = 0;
                int contador = 0;
            
                //Establecemos moneda por defecto
                string Moneda = System.Configuration.ConfigurationSettings.AppSettings.Get("Moneda");
            
                ////Cargamos el fichero de rates
                string FileRates = System.Configuration.ConfigurationSettings.AppSettings.Get("FileRates");
                string PathRates = System.Configuration.ConfigurationSettings.AppSettings.Get("PathRates");
                Conversiones = GNB.Datos.AccesoDatos.RecuperaArchivo(PathRates, FileRates);

                //Cargamos el fichero de transactions
                string FileTransactions = System.Configuration.ConfigurationSettings.AppSettings.Get("FileTransactions");
                string PathTransactions = System.Configuration.ConfigurationSettings.AppSettings.Get("PathTransactions");
                Transacciones = GNB.Datos.AccesoDatos.RecuperaArchivo(PathTransactions, FileTransactions);

                var selectTransacciones = from el in Transacciones.Elements("transaction")
                                          where (string)el.Attribute("sku") == sku
                                          select el;

                GNB.Logica.DataContracts.ListaTransacciones lista = new GNB.Logica.DataContracts.ListaTransacciones();
                GNB.Logica.DataContracts.transaction[] arrayTransaction = new GNB.Logica.DataContracts.transaction[selectTransacciones.Count()];

                if (selectTransacciones.Count() > 0)
                {
                    foreach (var transaccion in selectTransacciones)
                    {
                        //Solo si necesita conversión
                        if ((string)transaccion.Attribute("currency") != Moneda)
                        {
                            //Recuperamos la transacción convertida
                           ConvertirMoneda(transaccion, Conversiones, Moneda);
                        }

                        //Añadimos al objeto de salida
                        arrayTransaction[contador] = new GNB.Logica.DataContracts.transaction();
                        arrayTransaction[contador].amount = transaccion.Attribute("amount").Value;
                        arrayTransaction[contador].currency = transaccion.Attribute("currency").Value;
                        arrayTransaction[contador].sku = transaccion.Attribute("sku").Value;

                        contador++;
                        total += System.Convert.ToDecimal(transaccion.Attribute("amount").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                    }

                    //Añadimos el total y devolvemos el resultado
                    lista.transactions = arrayTransaction;
                    lista.total = total;
                }
                else
                {
                    Log.DebugFormat("No se han encontrado transacciones para el producto: {0}", sku);
                }

                return lista;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Se ha producido un error al listar las transacciones del producto: {0}", sku);
                Log.ErrorFormat("Detalle del error: {0}", ex.ToString());
                return null;
            }
        }

        /// <summary>Devuelve todas las transacciones</summary>
        /// <returns>XElement con las tasas</returns>
        public XElement ListarTransacciones()
        {

            try
            {

                //Cargamos el fichero de transactions
                string FileTransactions = System.Configuration.ConfigurationSettings.AppSettings.Get("FileTransactions");
                string PathTransactions = System.Configuration.ConfigurationSettings.AppSettings.Get("PathTransactions");
                XElement Transacciones = GNB.Datos.AccesoDatos.RecuperaArchivo(PathTransactions, FileTransactions);

                return Transacciones;

            }
            catch (Exception ex)
            {
                Log.Error("Se ha producido un error al listar las transacciones");
                Log.ErrorFormat("Detalle del error: {0}", ex.ToString());
                return null;
            }
        }

        /// <summary>Devuelve todas las tasas</summary>
        /// <returns>XElement con las tasas</returns>
        public XElement ListarTasas()
        {

            try
            {
                ////Cargamos el fichero de rates
                string FileRates = System.Configuration.ConfigurationSettings.AppSettings.Get("FileRates");
                string PathRates = System.Configuration.ConfigurationSettings.AppSettings.Get("PathRates");
                XElement converiones = GNB.Datos.AccesoDatos.RecuperaArchivo(PathRates, FileRates);

                return converiones;
            }
            catch (Exception ex)
            {
                Log.Error("Se ha producido un error al listar las conversiones");
                Log.ErrorFormat("Detalle del error: {0}", ex.ToString());
                return null;
            }
        }

        /// <summary>Convierte todos los resultados a la moneda indicada </summary>
        /// <returns>XElement de las transacciones convertidas</returns>
        /// <param name="transacciones">XElement con las transacciones a convertir</param>
        /// <param name="conversiones">XElement con las conversiones</param>
        /// <param name="moneda">string con la moneda a la que se desea convertir</param>
        public static void ConvertirMoneda(XElement transaccion, XElement conversiones, string moneda = "EUR")
        {
            decimal conversion1 = 0;
            decimal conversion2 = 0;
            decimal importe = 0;
            decimal total = 0;

            try
            {
                //Buscamos la conversión directa
                var conversion = from el in conversiones.Elements("rates")
                                 where el.Attribute("from") == transaccion.Attribute("currency")
                                 where (string)el.Attribute("to") == moneda
                                 select el;

                if (conversion.Count() == 0) //Si no hay conversión directa buscamos otra
                {
                    var primeraConversion = from el in conversiones.Elements("rate")
                                            where (string)el.Attribute("from") == (string)transaccion.Attribute("currency")
                                            select el;
                    //Si hay conversiones
                    if (primeraConversion.Count() > 0)
                    {
                        foreach (var element in primeraConversion) //Recorremos las conversiones por si hubiera más de una
                        {
                            if ((string)element.Attribute("to") != moneda)
                            {
                                var segundaConversion = from el in conversiones.Elements("rate")
                                                        where (string)el.Attribute("from") == (string)element.Attribute("to")
                                                        where (string)el.Attribute("to") == moneda
                                                        select el;
                                if (segundaConversion.Count() != 0) //Encuentra una segunda conversión
                                {
                                    conversion1 = System.Convert.ToDecimal(primeraConversion.FirstOrDefault().Attribute("rate").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                                    conversion2 = System.Convert.ToDecimal(segundaConversion.FirstOrDefault().Attribute("rate").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                                    importe = System.Convert.ToDecimal(transaccion.Attribute("amount").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                                    total = Math.Round(importe * conversion1 * conversion2, 2, MidpointRounding.ToEven);
                                }
                            }
                            else // Encuentra la primera conversión
                            {
                                conversion1 = System.Convert.ToDecimal(primeraConversion.FirstOrDefault().Attribute("rate").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                                importe = System.Convert.ToDecimal(transaccion.Attribute("amount").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                                total = Math.Round(importe * conversion1, 1, MidpointRounding.ToEven);
                            }
                        }
                        // Si no ha encontrado ninguna conversión, total será 0 y mostraremos un error
                        if (total == 0)
                        {
                            Log.ErrorFormat("No se han podido encontrar conversiones a {0} para la moneda: {1}", moneda, transaccion.Attribute("currency"));
                            return;
                        }
                    }
                    else //Si no hay conversiones para esa moneda devolvemos error
                    {
                        Log.ErrorFormat("No se han podido encontrar conversiones a {0} para la moneda: {1}", moneda, transaccion.Attribute("currency"));
                        return;
                    }
                }
                else //Encuentra una conversión directa
                {
                    conversion1 = System.Convert.ToDecimal(conversion.FirstOrDefault().Attribute("rate").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                    importe = System.Convert.ToDecimal(transaccion.Attribute("amount").Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                    total = Math.Round(importe * conversion1, 2, MidpointRounding.ToEven);
                }

                transaccion.Attribute("currency").SetValue(moneda);
                transaccion.Attribute("amount").SetValue(total.ToString(System.Globalization.CultureInfo.GetCultureInfo("en-US")));
                Log.DebugFormat("Se ha convertido la moneda a {0}", moneda); 
                return;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Se ha producido un error en ConvertirMoneda: {0}", ex.ToString());
                return;
            }
        }
    }

    public class Utilidades
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string Serializar(object lista, Type tipo)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(tipo);

                StringBuilder sb = new StringBuilder();
                TextWriter tw = new StringWriter(sb);
                serializer.Serialize(tw, lista);
                tw.Close();
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Se ha producido un error al serializar el objeto: {0}", ex.ToString());
                return "";
            }
        }
    }
}
